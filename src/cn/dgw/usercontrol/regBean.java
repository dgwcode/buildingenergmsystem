package cn.dgw.usercontrol;

/**
 * @author DGW
 * @date 2017 2017年4月25日 下午1:11:43
 * @filename userBean.java  
 */
public class regBean {
	/*
	 * 登录bean
	 * 用户名：
	 * 密码：
	 * QQ:
	 */
	private String username;
	private String password;
	private int qqnumber;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getQqnumber() {
		return qqnumber;
	}
	public void setQqnumber(int qqnumber) {
		this.qqnumber = qqnumber;
	}
	

}
