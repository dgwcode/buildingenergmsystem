package cn.dgw.usercontrol;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;
import java.util.Set;

import my.dbutils.jdbc.DbUtils;

/**
 * @author DGW
 * @date 2017 2017��4��25�� ����1:29:19
 * @filename addUser.java  
 */
public class CrudUser {
	/*
	 * ������ݿ����
	 */
	private regBean userinfo;
	private loginBean logininfo;
	private Connection connection;
	
	public void setUserinfo(regBean userinfo) {
		this.userinfo = userinfo;
	}
	
	public void setLogininfo(loginBean logininfo) {
		this.logininfo = logininfo;
	}
	public boolean queryuser() throws SQLException{
		connection = DbUtils.getConnection();
		String  sql="select user from userinfo where id";
		PreparedStatement statement = connection.prepareStatement(sql);
		ResultSet query = statement.executeQuery();
		while (query.next()) {
			if (query.getString("username").equals(userinfo.getUsername())) {
				return true;
			}
  		}
		DbUtils.close(statement);
		return false;
		
		
	}
	
	public void adduser() throws SQLException{
		connection = DbUtils.getConnection();
		String insert ="INSERT INTO userinfo (username,password,qqnumber) VALUES (?,?,?)" ;
		PreparedStatement pStatement = connection.prepareStatement(insert);
		pStatement.setString(1, userinfo.getUsername());
		pStatement.setString(2, userinfo.getPassword());
		pStatement.setLong(3, userinfo.getQqnumber());
		pStatement.addBatch();
		pStatement.executeBatch();
		DbUtils.close(pStatement);
	}
	
	public boolean login() throws SQLException{
		LinkedHashMap<String, String> login=new LinkedHashMap<>();
		Connection con=DbUtils.getConnection();
		String sql="SELECT username,password FROM userinfo WHERE id;";
		PreparedStatement statement = con.prepareStatement(sql);
		ResultSet query = statement.executeQuery();
		while (query.next()) {
			login.put(query.getString("username"), query.getString("password"));
		}
		Set<String> keySet = login.keySet();
		//�жϵ�¼��
		if (keySet.contains(logininfo.getName())) {
			//�ж����� 
			if (login.get(logininfo.getName()).equals(logininfo.getPasswd())) {
				return true;
			}
			return false;
		}
		DbUtils.close(statement);
		return false;
		
	}

}
